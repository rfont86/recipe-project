import { EventEmitter, Injectable } from '@angular/core';

import { Recipe } from './recipe.model';

@Injectable({providedIn: 'root',})
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    new Recipe('A test recipe', 'This is a simple test', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-Tjr9IZAUdeAaRLv25K4uwXOidl1XrzxAow&usqp=CAU'),
    new Recipe('Another test recipe', 'This is another simple test', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-Tjr9IZAUdeAaRLv25K4uwXOidl1XrzxAow&usqp=CAU')
  ];

  getRecipes() {
    return this.recipes.slice();
  }
}