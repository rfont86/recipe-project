import { EventEmitter, Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";

@Injectable({providedIn: 'root',})
export class ShoppongListService {
  ingredientsChanged = new EventEmitter<Ingredient[]>();

  ingredients: Ingredient[]= [
    new Ingredient('Apples', 5),
    new Ingredient('Tomates', 10),
  ];

  getIngredients() {
    return this.ingredients.slice();
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.ingredientsChanged.emit(this.ingredients.slice());
  }
}