import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'recipes-page';
  pageLoaded: string = 'recipes';

  onNavigate(page: string) {
    this.pageLoaded = page;
  }
}
